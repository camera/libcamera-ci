#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2023 Laurent Pinchart <laurent.pinchart@ideasonboard.com>
#
# Common functions for CI scripts

find_base() {
	# Find the common ancestor of two revisions r1 and r2, for the purpose
	# of testing each commit in a branch history individually. As a safety
	# check, return an error if the branch contains more than 50 new
	# commits.
	local r1=$1
	local r2=$2
	local base=$(git merge-base $r1 $r2)

	if [[ -z $base ]] ; then
		echo "No common ancestor between $r1 and $r2."
		echo "Make sure your master branch is up-to-date."
		exit 1
	fi

	if [[ $(git log --oneline $base..$r2 | wc -l) -gt 50 ]] ; then
		echo "Common ancestor $base of $r1 and $r2 is too far in history."
		echo "Make sure your master branch is up-to-date."
		exit 1
	fi

	echo $base
}

run() {
	local section=$1
	local collapsed

	[[ $2 == 'collapsed' ]] && collapsed='[collapsed=true]'

	echo -ne "section_start:`date +%s`:${section}${collapsed}\r\e[0K"
	$section
	echo -e "section_end:`date +%s`:${section}\r\e[0K"
}
