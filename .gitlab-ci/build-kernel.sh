#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2023 Laurent Pinchart <laurent.pinchart@ideasonboard.com>
#
# Build a kernel image suitable for running unit tests in a virtual machine.

set -e

source "$(dirname "$0")/lib.sh"

kernel_download() {
	echo "Download and unpack kernel sources"

	curl -LO https://cdn.kernel.org/pub/linux/kernel/v${KERNEL_VERSION/.*/}.x/linux-${KERNEL_VERSION}.tar.xz
	tar xf linux-${KERNEL_VERSION}.tar.xz
}

kernel_config() {
	echo "Configure the kernel"

	# Start with the architecture's defconfig, and add the kvm guest
	# options.
	make $(arch)_defconfig
	make kvm_guest.config

	# Disable unneeded features and enable the additional config options we
	# need.
	local disable=(
		USB
		SOUND
		SND
		DRM
		INPUT
		I2C
		HID
		CRYPTO
		IPV6
	)

	local enable=(
		MEMCG
		USER_NS
		FUSE_FS
		VIRTIO_FS
		OVERLAY_FS
		FB
		MEDIA_SUPPORT
		MEDIA_TEST_SUPPORT
		V4L_TEST_DRIVERS
		VIDEO_VIM2M
		VIDEO_VIMC
		VIDEO_VIVID
		UDMABUF
	)

	./scripts/config ${disable[@]/#/--disable }
	./scripts/config ${enable[@]/#/--enable }

	# .config has been Run olddefconfig to sanitize .config
	make olddefconfig
}

kernel_build() {
	echo "Build the kernel"

	local jobs=${FDO_CI_CONCURRENT:-4}
	make -j$jobs bzImage WERROR=0
}

target_dir=$1

run kernel_download

pushd linux-${KERNEL_VERSION}

run kernel_config
run kernel_build collapsed

mkdir -p "$target_dir"
cp arch/$(arch)/boot/bzImage "$target_dir/"
cp .config "$target_dir/"

popd
