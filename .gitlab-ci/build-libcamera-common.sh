#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2023 Laurent Pinchart <laurent.pinchart@ideasonboard.com>
#
# Common functions to build libcamera

set -e

PKGS_CLANG_NATIVE_DEPS=(
	libc++-dev
	libc++abi-dev
)

# Some of the packages required to compile libcamera are not multiarch-capable,
# preventing multiple arch variants from coexisting in the same image. This
# libevent-dev (see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=895142),
# and libgstreamer-plugins-base1.0-dev (see
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1016631). We need to
# install the arch variant just before building libcamera.
#
# If this becomes problematic in the future, possible alternatives are creating
# different container images for different architectures, or switching to a
# different cross-compilation environment (likely based on buildroot).

PKGS_LIBCAMERA_NATIVE_DEPS=(
	libevent-dev
	libgstreamer-plugins-base1.0-dev
)

libcamera_install_pkgs() {
	echo "Installing non-multiarch dependencies"

	local pkgs=( )

	for pkg in ${PKGS_LIBCAMERA_NATIVE_DEPS[@]} ; do
		pkgs+=( $pkg${ARCH:+:${ARCH}} )
	done

	# The libc++ packages are also not multiarch-capable. Additionally,
	# they conflict with the GStreamer packages, due to usage of
	# incompatible libunwind implementations, as well as with the
	# system-provided libgtest, due to usage of a different C++ standard
	# library. When using libc++, disable GStreamer support in libcamera,
	# and force usage of the wrap to provide libgtest.

	if [[ $CC == clang ]] ; then
		for pkg in ${PKGS_CLANG_NATIVE_DEPS[@]} ; do
			pkgs+=( $pkg${ARCH:+:${ARCH}} )
		done

		MESON_OPTIONS="$MESON_OPTIONS -D gstreamer=disabled -D force_fallback_for=gtest"
	fi

	apt update
	apt install -y ${pkgs[@]}
}

libcamera_setup() {
	echo "Setting up libcamera build with meson options $MESON_OPTIONS"

	local cross_file

	if [[ -n $ARCH && $ARCH != amd64 ]] ; then
		cross_file="--cross-file /opt/meson/cross-compilation-${ARCH}.conf"
	fi

	meson setup build \
		$cross_file \
		--wrap-mode=nofallback \
		-D buildtype=$BUILD_TYPE \
		$MESON_OPTIONS
}
