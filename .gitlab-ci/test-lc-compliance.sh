#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2024 Google Inc.

set -e

source "$(dirname "$0")/lib.sh"

libcamera_compliance() {
	echo "Running libcamera compliance tests in a qemu VM"

	virtme-ng \
		--verbose \
		--skip-modules \
		--force-9p \
		--rwdir "$PWD/build" \
		--run /opt/linux/bzImage \
		--exec " \
			set -x; \
			mv /usr/local/share/libcamera/pipeline/virtual/virtual.yaml.example \
			   /usr/local/share/libcamera/pipeline/virtual/virtual.yaml; \
			LIBCAMERA_LOG_LEVELS=*:DEBUG \
			ASAN_OPTIONS=halt_on_error=1:abort_on_error=1:print_summary=1 \
			UBSAN_OPTIONS=halt_on_error=1:abort_on_error=1:print_summary=1:print_stacktrace=1 \
			GTEST_OUTPUT=xml:./build/lc-compliance-report.xml \
			lc-compliance -c Virtual0; \
			echo \\\$? > ./build/.test-status; \
		"

	local status=$(cat build/.test-status)
	echo "Test result exit state: $status"
	rm build/.test-status

	if [[ $status != 0 ]] ; then
		exit $status
	fi
}

run libcamera_compliance
