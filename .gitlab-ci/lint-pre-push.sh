#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2023 Laurent Pinchart <laurent.pinchart@ideasonboard.com>
#
# Pre-merge checks

set -e

source "$(dirname "$0")/lib.sh"

libcamera_premerge() {
	echo "Running Pre-merge checks for $CI_COMMIT_REF_NAME ($base..$CI_COMMIT_SHA)"

	# Wrap parameters as git would send them to the pre-push hooks directly.
	echo "$CI_COMMIT_REF_NAME $CI_COMMIT_SHA refs/heads/integration/pre-push-lint-test $base" \
		| ./utils/hooks/pre-push origin "$CI_DEFAULT_BRANCH"
}

base=$(find_base origin/$CI_DEFAULT_BRANCH $CI_COMMIT_SHA)

if [[ $base == $CI_COMMIT_SHA ]] ; then
	echo "No commit to test, skipping"
	exit 0
fi

run libcamera_premerge
