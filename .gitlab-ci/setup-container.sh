#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2023 Laurent Pinchart <laurent.pinchart@ideasonboard.com>
#
# Setup the container.

set -e

source "$(dirname "$0")/lib.sh"

# CBUILD packages are used only when building the container, while RUNTIME
# packages are used during the container runtime (which covers building
# libcamera).

PKGS_KERNEL_CBUILD=(
	bc
	bison
	curl
	file
	make
	flex
	gcc
	libelf-dev
	libssl-dev
	tar
	xz-utils
)

PKGS_LIBCAMERA_CBUILD=(
	dpkg-dev
	debhelper-compat
	build-essential
)

PKGS_LIBCAMERA_RUNTIME=(
	clang
	clang-format
	cmake
	doxygen
	g++
	ghostscript
	graphviz
	meson
	ninja-build
	openssl
	pkg-config
	pybind11-dev
	python3-autopep8
	python3-dev
	python3-jinja2
	python3-ply
	python3-sphinx
	python3-yaml
	shellcheck
	texlive-latex-extra
	qt6-tools-dev-tools
)

PKGS_LIBCAMERA_RUNTIME_MULTIARCH=(
	gstreamer1.0-plugins-base
	libdrm-dev
	libdw-dev
	libexif-dev
	libgstreamer1.0-dev
	libgtest-dev
	libjpeg-dev
	liblttng-ust-dev
	libpython3-dev
	libqt6opengl6-dev
	libsdl2-dev
	libssl-dev
	libtiff-dev
	libudev-dev
	libunwind-dev
	libyaml-dev
	lttng-tools
	qt6-base-dev
	nlohmann-json3-dev
)

PKGS_VIRTME_CBUILD=(
	cargo
	rustc
)

PKGS_VIRTME_RUNTIME=(
	python3-argcomplete
	python3-pip
	python3-requests
	qemu-system-x86
	systemd-standalone-tmpfiles
	udev
)

archs=( amd64 )

declare -A components

# Install additional packages on a per distribution version basis.
case $FDO_DISTRIBUTION_VERSION in
'bullseye')
	# gcc 9 to expand compilation testing coverage.
	PKGS_LIBCAMERA_RUNTIME+=( g++-9 )
	;;
'bookworm')
	# libclang-rt-dev for the clang ASan runtime.
	PKGS_LIBCAMERA_RUNTIME_MULTIARCH+=( libclang-rt-dev )
	# For cam and lc-compliance
	# libevent-dev cannot be used here, see build-libcamera-common.sh
	PKGS_LIBCAMERA_RUNTIME_MULTIARCH+=( libevent-2.1-7 libevent-pthreads-2.1-7 )
	;;
'trixie')
	# gcc 13 to expand compilation testing coverage.
	PKGS_LIBCAMERA_RUNTIME+=( g++-13 )
	# libclang-rt-dev for the clang ASan runtime.
	PKGS_LIBCAMERA_RUNTIME_MULTIARCH+=( libclang-rt-dev )
	;;
esac

# We use Debian bookworm containers to produce ARM binaries and run unit tests
# with virtme, and other Debian versions for compilation-testing on amd64 only.
if [[ $FDO_DISTRIBUTION_VERSION == 'bookworm' ]] ; then
	archs+=( arm64 armhf )
	components['kernel']=1
	components['virtme']=1
fi

pkgs_cbuild=( ${PKGS_LIBCAMERA_CBUILD[@]} )
pkgs_runtime_native=( ${PKGS_LIBCAMERA_RUNTIME[@]} )
pkgs_runtime_multiarch=( ${PKGS_LIBCAMERA_RUNTIME_MULTIARCH[@]} )

for component in ${!components[@]} ; do
	declare -n pkgs=PKGS_${component@U}_CBUILD
	pkgs_cbuild+=( ${pkgs[@]} )
	declare -n pkgs=PKGS_${component@U}_RUNTIME
	pkgs_runtime_native+=( ${pkgs[@]} )
done

cbuild_install_pkgs() {
	echo "Installing distribution packages"

	local arch

	# The qt6-tools-dev-tools package is only available for bullseye in the
	# backports repository. Enable backports only for that version.
	if [[ $FDO_DISTRIBUTION_VERSION == 'bullseye' ]] ; then
		echo "deb http://deb.debian.org/debian bullseye-backports main" \
		       > /etc/apt/sources.list.d/bullseye-backports.list
		apt update
	fi

	apt install -y ${pkgs_cbuild[@]}
	apt-mark auto ${pkgs_cbuild[@]}

	apt install -y ${pkgs_runtime_native[@]}

	for arch in ${archs[@]} ; do
		dpkg --add-architecture $arch
	done

	apt update

	for arch in ${archs[@]} ; do
		local pkgs=( crossbuild-essential-$arch )

		for pkg in ${pkgs_runtime_multiarch[@]} ; do
			pkgs+=( $pkg:$arch )
		done

		apt install -y ${pkgs[@]}
	done
}

dpkg_version() {
	local pkg=$1

	local version=$(dpkg -s $pkg | grep '^Version: ')
	version=${version#* }
	version=${version%%+*}
	version=${version%%-*}
	echo $version
}

dpkg_check_version() {
	local version=$1
	local min_version=$2

	[[ $(echo -e "${version}\n${min_version}" | sort -V | head -n1) != $min_version ]]
}

cbuild_fixups() {
	echo "Applying miscellaneous fixups"

	local min_version
	local pip3_options
	local version

	if [[ $FDO_DISTRIBUTION_VERSION != 'bullseye' ]] ; then
		pip3_options=--break-system-packages
	fi

	# Install meson from pip.
	min_version=1.2.0
	version=$(dpkg_version meson)
	if dpkg_check_version $version $min_version ; then
		echo "meson $version too old, installing $min_version from pip"

		apt remove -y meson
		apt install -y python3-pip
		pip3 install ${pip3_options} meson==${min_version}
	fi

	# Install pygments from pip.
	min_version=2.10.0
	version=$(dpkg_version python3-pygments)
	if dpkg_check_version $version $min_version ; then
		echo "pygments $version too old, installing $min_version from pip"

		apt install -y python3-pip
		pip3 install ${pip3_options} pygments==${min_version}
	fi

	# Create pybind11.pc manually if not provided by the distribution
	# package.
	if [[ ! -f /usr/share/pkgconfig/pybind11.pc ]] ; then
		echo "pybind11-dev package not providing pybind11.pc, creating file manually"

		version=$(dpkg_version pybind11-dev)

		cat <<EOF > /usr/share/pkgconfig/pybind11.pc
prefix=/usr
includedir=\${prefix}/include

Name: pybind11
Description: Seamless operability between C++11 and Python
Version: ${version}
Cflags: -I\${includedir}
EOF
	fi
}

cbuild_cleanup_pkgs() {
	echo "Cleaning up distribution packages"

	apt autoremove -y
	apt-get clean
}

cbuild_generate_cross_files() {
	echo "Generating meson cross files"

	local arch

	mkdir -p /opt/meson/

	for arch in ${archs[@]} ; do
		meson env2mfile --debarch $arch --cross \
			-o /opt/meson/cross-compilation-$arch.conf
	done
}

run cbuild_install_pkgs
run cbuild_fixups

run cbuild_generate_cross_files

if [[ -n ${components['kernel']} ]] ; then
	.gitlab-ci/build-kernel.sh /opt/linux/
fi

if [[ -n ${components['virtme']} ]] ; then
	.gitlab-ci/install-virtme.sh
fi

# Always build libpisp for each container possible.
for arch in ${archs[@]} ; do
	.gitlab-ci/build-libpisp-deb.sh $arch
done

run cbuild_cleanup_pkgs
