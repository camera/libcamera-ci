#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2025 Kieran Bingham <kieran.bingham@ideasonboard.com>
#
# Build libpisp as a dependency to libcamera

set -ex

source "$(dirname "$0")/lib.sh"

BUILD_CONCURRENT_JOBS=${BUILD_CONCURRENT_JOBS:-1}
BUILD_TYPE=${BUILD_TYPE:-release}

ARCH=$1

libpisp_sources() {
	if [[ -e libpisp ]] ; then return ; fi ;

	# Temporary hosting of combined debian rules and source
	git clone https://github.com/kbingham/libpisp.git -b kbingham/ci
}

libpisp_setup() {
	echo "Installing libpisp dependencies"
	apt install -y --no-install-recommends \
		meson \
		debhelper-compat
}

libpisp_build() {
	echo "Building libpisp packages ${ARCH:+for $ARCH}"
	dpkg-buildpackage -us -uc ${ARCH:+-a $ARCH}
}

libpisp_install() {
	echo "Installing libpisp"
	dpkg -i ../libpisp*.deb
}

run libpisp_sources collapsed

pushd libpisp

run libpisp_setup collapsed
run libpisp_build collapsed
run libpisp_install

popd
