#!/bin/bash
# SPDX-License-Identifier: GPL-3.0-or-later
#
# SPDX-FileCopyrightText: © 2023 Ideas On Board Oy
# Laurent Pinchart <laurent.pinchart@ideasonboard.com>

set -ex

echo "Building Chrome OS SDK for ${cros_board}"

# Build SDK and setup the board. Specify the 'dev' USE flag for the libcamera
# package to enable compilation of the development and test tools.
cros_sdk --create
cros telemetry --disable
cros_sdk setup_board --board=${cros_board}

sudo bash -c "echo 'dev-libs/libevent threads' >> out/build/${cros_board}/etc/portage/package.use"
sudo bash -c "echo 'media-libs/libcamera-upstream dev' >> out/build/${cros_board}/etc/portage/package.use"

cros workon --board ${cros_board} start media-libs/libcamera-upstream

# Fix ownership of files in the '.cache' directory. This fixes write permission
# errors when ccache tries to write to its cache. The issue is believed to be
# caused by the hack that extracts tarballs with --no-same-owner (see stage1),
# but this hasn't been investigated further.
sudo chown -R cros /home/cros/chromiumos/.cache/

# Build the packages needed for libcamera.
cros build-packages \
	--accept-license="*" \
	--board=${cros_board} \
	--jobs=${num_jobs} \
	--no-withautotest \
	--no-withdebugsymbols \
	--no-withdev \
	--no-withfactory \
	--no-withtest \
	media-libs/libcamera-upstream \
	sys-libs/libcxx

mkdir /home/cros/chromiumos/out/cros-sdk/

# Generate a sysroot containing all the dependencies needed to build libcamera.
# The sysroot needs sys-libs/libcxx in addition to
# media-libs/libcamera-upstream as it is not listed as an explicit dependency
# of the libbcamera-upstream package.
cros_sdk cros_generate_sysroot --board ${cros_board} \
	--out-dir /mnt/host/out/cros-sdk/ \
	--package 'media-libs/libcamera-upstream sys-libs/libcxx'

# Generate the toolchain. cros_setup_toolchains needs to run as root due to
# b/282231712.
cros_sdk sudo cros_setup_toolchains --create-packages \
	--output-dir /mnt/host/out/cros-sdk/ \
	--target x86_64-cros-linux-gnu

# Split the libcamera-upstream.tbz2 into the .tar.bz2 archive and the XPAK. The
# XPAK is needed in the final SDK image.
cros_sdk qtbz2 -s /build/${cros_board}/packages/media-libs/libcamera-upstream-9999.tbz2 \
	/mnt/host/out/cros-sdk/libcamera-upstream-9999.tar.bz2 \
	/mnt/host/out/cros-sdk/libcamera-upstream-9999.xpak

# Unpack the tarballs and copy the XPAK to make the SDK available for COPY in
# the final image.
mkdir /home/cros/cros-sdk
cd /home/cros/cros-sdk
mkdir sysroot toolchain
tar -xf /home/cros/chromiumos/out/cros-sdk/sysroot_media-libs_libcamera-upstream.tar.xz -C sysroot/
tar -xf /home/cros/chromiumos/out/cros-sdk/x86_64-cros-linux-gnu.tar.xz -C toolchain/

cp /home/cros/chromiumos/out/cros-sdk/libcamera-upstream-9999.xpak .
